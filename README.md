This is a project housing the code I cobbled together to make it so I can manage background music for my tabletop rpg group that's being done remotely.

It's licensed GPLv3, if you don't like it,then just re-write it yourself. This wasn't a very complicated project.

This isn't a community project and I don't intend it to be. If you want it to be then feel free to fork it and take it that way.