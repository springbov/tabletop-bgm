function getSettings()
{
  return fetch("/settings.json")
    .then(r => r.json())
}

/** @type {HTMLAudioElement} */
const audioPlayer = document.getElementById("player");
/** 
 * @type {object} 
 * */
let settings;

let currentTrack = 0;

async function getCurrentTrack()
{
  return fetch("/currentBGM?" + Date.now())
    .then(x => x.text())
    .then(x => x)
}

let firstPlay = true;
function onFirstPlay()
{
  if (firstPlay)
    audioPlayer.volume = 0.6;
  
  firstPlay = false;
}

async function foreverUpdateTrack()
{
  let newTrack = await getCurrentTrack();
  console.log(typeof newTrack)
  if (typeof newTrack != "undefined" && newTrack != currentTrack)
  {
    currentTrack = newTrack
    let currentFile = settings.bgm.filter(x => x.id == currentTrack)[0].file
    audioPlayer.setAttribute("src", `bgm/${currentFile}`)
    onFirstPlay();
    audioPlayer.play();
  }

  setTimeout(foreverUpdateTrack, 5000);
}

async function main()
{
  settings = await getSettings();
  audioPlayer.loop = true;
  audioPlayer.play();

  foreverUpdateTrack()
}

main();