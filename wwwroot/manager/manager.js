function getSettings()
{
  return fetch("/settings.json")
    .then(r => r.json())
}

/**
 * Sets the track
 * @param {number} track 
 */
function setTrack(track)
{
  fetch("/admin/setBGM", {
    method: "post",
    body: track
  })
  .catch(x => console.error("Ruh roh", x))
}

function makePanel(setting)
{
  /** @type {HTMLDivElement} */
  let div = document.createElement("div");

  /** @type {HTMLButtonElement} */
  let button = document.createElement("button")
  button.innerText = "Set Track"
  button.addEventListener("click", () => setTrack(setting.id))

  /** @type {HTMLLabelElement} */
  let label = document.createElement("label")
  label.innerText = setting.file;

  div.appendChild(label)
  div.appendChild(button)

  return div
}

/** @type {HTMLDivElement} */
const controlPanel = document.getElementById("controlPanel")

async function main()
{
  const settings = await getSettings();
  settings
    .bgm
    .map(x => makePanel(x))
    .forEach(x => {
      controlPanel.appendChild(x)
    })
}

main()