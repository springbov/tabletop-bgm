import express from "express";
import bgmsettings from "./map.json"
import bodyParser from "body-parser";

const app = express();
const port = 3000;

let currentBGMIndex: number = 0;

app.use(express.static("wwwroot"))

app.get("/settings.json", (req, res) => {
  res.send(JSON.stringify(bgmsettings));
})

app.post("/admin/setBGM", bodyParser.text(), (req, res) => {
  let index = parseInt(req.body)
  console.log("req body", req.body)
  if (!isNaN(index))
  {
    console.log("setting index too", index)
    currentBGMIndex = index
  }
  res.status(200);
  res.send();
})

app.get("/currentBGM", (req, res) => {
  res.send(currentBGMIndex.toString());
})

app.listen(port, () => console.log(`Now listening at http://localhost:${port}`))
